require_relative 'lib/vow'

Gem::Specification.new do |s|
  s.name     = 'vow'
  s.version  = Vow::VERSION
  s.summary  = 'system() call wrapper that makes it convenient to execute external commands in safe manner'
  s.author   = 'Ruslan Nugmanov'
  s.email    = 'ruslan@nugmanov.net'
  s.files    = ['lib/vow.rb', 'lib/vow/helper.rb']
  s.license  = 'ISC'
  s.homepage = 'https://gitlab.com/nugmanov/vow'
end
