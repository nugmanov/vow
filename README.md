# Vow

## Description

Vow is a `Kernel.system()` wrapper.
Vow makes it more convenient to construct external commands to run and deal with their exit codes.

Using `Kernel.system()` with standard shell can lead to command injection.
Consider the following artificial example where `message` is controlled by an attacker:

    > message = '; rm /I_could_do_anything'
    > system("echo #{message}")
    => false
    rm: /I_could_do_anything: No such file or directory

Since the whole string is parsed by the shell, interpolating untrusted data into `commandline` argument can be dangerous.
The simplest way to avoid this issue is to split the command name and its arguments making sure `Kernel.system()` is not using the shell:

    > system('echo', message)
    ; rm /I_could_do_anything
    => true

However, using this form can be cumbersome, especially if you need to construct your command based on conditions.

### system() replacement

`Vow.system()` is a drop-in replacement for the `Kernel.system` with the following changes:

* All positional arguments are flattened.

        # These commands are identical:
        Vow.system('command', ['a', 'b'], 'c')
        Vow.system('command', 'a', 'b', 'c')

* `nil`'s are silently removed. This is to be able to construct commands as following:

        Vow.system('command', '-v' if verbose, 'argument')
        Vow.system('sudo' if use_sudo, 'command', 'argument')

* Cmdline (first non-nil positional argument) is checked to make sure it doesn't have whitespaces or some shell metacharacters.
This is not intended to be a security measure but to prevent unintentinal misuse and to encorage passing the command name and its arguments separately.
TODO: be able to turn this check off.

See module docs for the full documentation for the method and all its keyword arguments.

### additional methods

There are additional methods that can be used as shortcuts:

* `Vow.strict` - raise an exception if the command couldn't be ran or exited with an unexpected exit code (expected to be 0 by default)

* `Vow.silent` - suppress stdout and stderr output.

## Examples

    # run an adhoc command and raise an exception if it failed:
    Vow.system('apt-get', command)

    # delete a user if it exists:
    Vow.silent('id', user) and Vow.system('userdel', user)

## Installation

Using rubygems:

    gem install vow
