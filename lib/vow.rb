###
# = Overview
#
# Vow is a Kernel.system() wrapper.
# Vow makes it more convenient to construct external commands to run and deal with their exit codes.
#
# = Usage
#
# In the simplest form, Vow.system() can be used as a drop-in replacement for Kernel.system():
#
#   # Execute command
#   Vow.system('echo', 'Hello, World!')

module Vow
  VERSION = '0.1.1'

  def self.system(*command, **args)
    may_raise = args.delete(:may_raise)

    expected_exit_codes = (args.delete(:returns) or 0)
    expected_exit_codes = [expected_exit_codes].flatten

    command = Helper.refine_command(command)

    if Kernel.system(*command, **args).nil?
      if may_raise
	raise "Failed to execute command: #{Helper.printable_command(command)}"
      else
	return nil
      end
    end

    exit_code = $?.exitstatus
    if expected_exit_codes.include?(exit_code)
      return true
    else
      if may_raise
	raise "Unexpected error code (#{exit_code}) from command: #{Helper.printable_command(command)}"
      else
	return false
      end
    end
  end

  def self.silent(*command, **args)
    args[:out] = '/dev/null'
    args[:err] = '/dev/null'
    self.system(*command, **args)
  end

  def self.strict(*command, **args)
    args[:may_raise] = true
    self.system(*command, **args)
  end
end

require_relative 'vow/helper'
