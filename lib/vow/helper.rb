module Vow::Helper
  def self.refine_command(command)
    refined = command.flatten.select {|a| !a.nil?}

    if refined.length == 0
      raise "Nothing to run"
    end

    cmdname = refined[0]
    if cmdname.include?(' ')
       raise "Command name does not follow safe pattern: '#{cmdname}'"
    end

    refined
  end

  def self.printable_command(command)
    simple_command = true
    command.each do |word|
      unless %r|^[[[:alnum:]].+_/-]+$|.match?(word)
	simple_command = false
	break
      end
    end

    if simple_command
      return command.join(' ')
    else
      return command.to_s
    end
  end
end
